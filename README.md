Package for MD Labs authentication


**Installation Steps**

*  Create a new laravel project
    
    `$ composer create-project --prefer-dist laravel/laravel mdlabs-auth`

*  Install mdlabs/auth package
    
    `$ composer require mdlabs/auth`

*  Add database configuration to config/app.php file
    

    ```<?php
        return [
        
            ...
            
            'providers' => [
                ...
            ],
            'aliases' => [
                ...
            ],
            
            'mobiauth' => [
                'status' => array("Active", "Inactive"),
                'types' => array("Customer" => "Active", "Restaurant" => "Active", "Shift Manager" => "Active"),
                'user' => array("default_type" => "Customer", "default_status" => "Active"),
                'aud' => "iorder-5246c",
                'publickeyurl' => 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com',
                'encalgorithm' => 'RS256'
            ],
            
        ];```
        
*  Note
    *  status, types and user are an array of a variables attributes in database columns so it is depend on project requirements,
    aud => is a firebase project id learn how to get it from: 
    [https://firebase.google.com/docs/projects/learn-more#the_and_the_project_id](url)
    *  publickeyurl and encalgorithm is a constants
    
    
*  Add package routes configuration code in app/Providers/RouteServiceProvider.php file

    ```
    class RouteServiceProvider extends ServiceProvider
    {
        ...
        public function map()
        {
            ...
            
            
            $this->mapMobiAuthRoutes();
            //
        }
        
        ...
        protected function mapMobiAuthRoutes()
        {
            if(isset($_SERVER['REMOTE_ADDR']) and $_SERVER['REMOTE_ADDR'] == "127.0.0.1")
                Route::middleware('api')->namespace('')->group(base_path('vendor\mdlabs\auth\routes.php'));
            else
                Route::middleware('api')->namespace('')->group(base_path('vendor/mdlabs/auth/routes.php'));
        }

    }
    ```


*  in composer.json you must define package classes and database migrations

    ```
    "autoload": {
            ...
            "classmap": [
                ...
                "vendor/mdlabs/auth"
            ]
            ...
        },
        
        ...
        
    "scripts": {
        "post-autoload-dump": [
            ...,
            
            "@php artisan migrate --path=/vendor/mdlabs/auth/migrations",
            "@php artisan migrate",
            "@php artisan passport:install"
            
            ],
        }
    ```


*  last step you must edit .env to connect to database project after that just write command:

    `$ composer dump-autoload`

        