<?php
Route::group(['prefix' => 'api'], function () {
    Route::post('login', 'Mobidev\Auth\MobiAuthController@login');
    Route::post('register', 'Mobidev\Auth\MobiAuthController@register');
    Route::post('registerorlogin', 'Mobidev\Auth\MobiAuthController@registerOrLogin');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Mobidev\Auth\MobiAuthController@logout');
        Route::get('user', 'Mobidev\Auth\MobiAuthController@user');
    });
});
